import org.jetbrains.kotlin.konan.properties.Properties

plugins {
    id("com.android.application")
    kotlin("android")
    id("dagger.hilt.android.plugin")
    id("kotlin-kapt")
}

android {

    val properties = Properties()
    properties.load(project.rootProject.file("local.properties").inputStream())
    val passKey = properties.getProperty("PASS", "")

    signingConfigs {
        getByName("debug") {
            storeFile =
                file("../keystore.jks")
            storePassword = "$passKey"
            keyPassword = "$passKey"
            keyAlias = "key0"
        }
    }
    compileSdk = Android.compileSdk

    defaultConfig {
        applicationId = Android.appId
        minSdk = Android.minSdk
        targetSdk = Android.targetSdk
        versionCode = Android.versionCode
        versionName = Android.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles (
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "$project.rootDir/proguard-rules.pro"
            )
        }
        getByName("debug") {
            applicationIdSuffix = ".debug"
            isDebuggable = true
        }
    }

    buildFeatures {
        dataBinding = true
    }

    flavorDimensions += "version"
    productFlavors{
        create("Test") {
            dimension = "version"
            applicationIdSuffix = ".Test"
            versionNameSuffix = "-Test"
            versionCode = 10000 + (versionCode ?: 0)
            buildConfigField("String", "API_URL", "\"test.api.example.com\"")
        }
        create("Stage") {
            dimension = "version"
            applicationIdSuffix = ".Stage"
            versionNameSuffix = "-Stage"
            versionCode = 20000 + (versionCode ?: 0)
            buildConfigField("String", "API_URL", "\"stage.api.example.com\"")

        }
        create("Porod") {
            dimension = "version"
            applicationIdSuffix = ".Porod"
            versionNameSuffix = "-Porod"
            versionCode = 30000 + (versionCode ?: 0)
            buildConfigField("String", "API_URL", "\"api.example.com\"")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"

        @Suppress("SuspiciousCollectionReassignment")
        freeCompilerArgs += "-opt-in=kotlin.RequiresOptIn"
    }
    kapt {
        correctErrorTypes = true
        useBuildCache = true
    }
}

dependencies {
    // Core
    implementation(AndroidX.Core.core)

    // Appcompat
    implementation(AndroidX.Appcompat.appcompat)

    //Google
    implementation(Google.Material.material)

    // Lifecycle
    implementation(AndroidX.Lifecycle.livedata)
    implementation(AndroidX.Lifecycle.viewmodel)

    // Activity
    implementation(AndroidX.Activity.activity)

    // Hilt
    implementation(HiltDagger.android)
    kapt(HiltDagger.compiler)

    // Test
    testImplementation(Test.Junit.junit)
    androidTestImplementation(Test.Espresso.espresso)
    androidTestImplementation(Test.Androidx.junitEtx)
}