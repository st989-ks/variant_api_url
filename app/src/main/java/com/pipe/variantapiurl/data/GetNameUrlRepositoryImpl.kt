package com.pipe.variantapiurl.data

import com.pipe.variantapiurl.BuildConfig
import com.pipe.variantapiurl.domain.GetNameUrlRepository
import javax.inject.Inject

class GetNameUrlRepositoryImpl @Inject constructor() : GetNameUrlRepository {
    override fun invoke() = BuildConfig.API_URL
}