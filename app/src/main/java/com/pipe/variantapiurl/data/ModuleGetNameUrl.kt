package com.pipe.variantapiurl.data

import com.pipe.variantapiurl.domain.GetNameUrlRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class ModuleGetNameUrl {

    @Provides
    fun bindDataSource(
    ): GetNameUrlRepository =
        GetNameUrlRepositoryImpl()
}