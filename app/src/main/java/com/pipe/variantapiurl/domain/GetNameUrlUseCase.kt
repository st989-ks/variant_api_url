package com.pipe.variantapiurl.domain

import com.pipe.variantapiurl.presenter.entity.NameUrl
import javax.inject.Inject

class GetNameUrlUseCase @Inject constructor(
    private val getNameUrlRepository: GetNameUrlRepository
) {
    fun invoke() = NameUrl(getNameUrlRepository.invoke())
}