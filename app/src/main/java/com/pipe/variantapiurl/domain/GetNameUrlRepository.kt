package com.pipe.variantapiurl.domain

interface GetNameUrlRepository {
    fun invoke(): String
}