package com.pipe.variantapiurl.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.pipe.variantapiurl.BuildConfig
import com.pipe.variantapiurl.R
import com.pipe.variantapiurl.databinding.FragmentMainBinding
import com.pipe.variantapiurl.presenter.MainViewModel
import com.pipe.variantapiurl.presenter.entity.NameUrl
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainFragment: Fragment()  {

    companion object {
        fun newInstance() = MainFragment()
    }

    @Inject
    lateinit var mainViewModel: MainViewModel

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModel.getNameUrlData().observe(viewLifecycleOwner) { res -> binding.data = res }
        mainViewModel.foundNameUrl()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding?.unbind()
    }
}