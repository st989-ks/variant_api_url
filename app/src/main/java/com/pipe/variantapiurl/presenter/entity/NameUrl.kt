package com.pipe.variantapiurl.presenter.entity

data class NameUrl(
    val urlStr: String
)