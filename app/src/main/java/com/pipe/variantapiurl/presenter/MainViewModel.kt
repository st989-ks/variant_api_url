package com.pipe.variantapiurl.presenter

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pipe.variantapiurl.domain.GetNameUrlUseCase
import com.pipe.variantapiurl.presenter.entity.NameUrl
import kotlinx.coroutines.*
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val getNameUrlUseCase: GetNameUrlUseCase,
) : ViewModel() {

    private val nameUrl = MutableLiveData<NameUrl>()

    fun getNameUrlData() = nameUrl

    fun foundNameUrl(){
        getNameUrlData().postValue(
            getNameUrlUseCase.invoke()
        )
    }
}